<?php require_once "config.php" ?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> ToDos </title> 
</head>
<body>
    <div class="titleDiv">
        <h1 class="Title">ToDos List <span class="material-icons md-48"> subject </span> </h1>
    </div>
    <input class="inputText" type="text"> 
    <button class="addButton"> <span class="material-icons md-34"> add_box</span> </button>

    <ul class="todosList">

        <li class="todosElement"> test 
            <input class="checkBox" type="checkbox">
            <button class="delButton"><span class="material-icons md-36"> delete_forever </span></button> 
        </li>

        <li class="todosElement"> test 2
            <input class="checkBox" type="checkbox">
            <button class="delButton"><span class="material-icons md-36"> delete_forever </span></button> 
        </li>

        <li class="todosElement"> test 3
            <input class="checkBox" type="checkbox">
            <button class="delButton"><span class="material-icons md-36"> delete_forever </span></button> 
        </li>
    </ul>
</body>
</html>